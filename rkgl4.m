function result = rkgl4(f, tSpan, initVal, nSteps) 
 
name = 'RKGL^{[4]}'; 
cost = 36; %5-7 iteration 30-42 products
 
a11 = 0.25; 
a12 = 0.25 - sqrt(3) / 6; 
 
a21 = 0.25 + sqrt(3) / 6; 
a22 = 0.25; 
 
b1 = 0.5; 
b2 = 0.5; 
 
c1 = a11 + a12; 
c2 = a21 + a22; 
 
n = size(f(0)); 
I = eye(n); 
Z = zeros(n); 
 
t0 = tSpan(1); 
tf = tSpan(2); 
h = (tf - t0) / nSteps; 
 
result = initVal; 
t = t0; 
 
for step = 1:nSteps 
I = eye(n); 
A1 = [[Z, h * I]; [h * f(t+c1*h), Z]]; 
A2 = [[Z, h * I]; [h * f(t+c2*h), Z]]; 
 
AA = [a11 * A1, a12 * A2; ... 
a21 * A1, a22 * A2;];
bA = [b1 * A1, b2 * A2]; 
 
I = eye(2*n); 
temp = (I + bA * ((eye(2*2*n) - AA)\[I, I]')); 
result = temp * result; 
 
t = t + h; 
end 
end 
 
