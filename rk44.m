function [result, cost, name] = rk44(f, tSpan, initVal, nSteps)
%RK76 solves a DE using Runge Kutta 6th-order 7-stage method
%   RK76 solves a differential equation of the type Y'=F(T) for TSPAN =
%   [T0; Tf] using NSTEPS with Y(T0) = INITVAL.

cost = 14;
name = '\mathrm{RK}_7^{[6]}';

t0 = tSpan(1);
tf = tSpan(2);
h = (tf - t0) / nSteps;

a21 = 0.5;
a31 = 0;
a32 = 0.5;
a41 = 0;
a42 = 0;
a43 = 1;

c2 = 0.5;
c3 = 0.5;
c4 = 1;

b1 = 1 / 6;
b2 = 2 / 6;
b3 = 2 / 6;
b4 = 1 / 6;

I = eye(size(f(0)));
Z = zeros(size(f(0)));
A = @(t) [[Z, I]; [f(t), Z]];

t = t0;
X = initVal;
A3 = A(t0);
for j = 1:nSteps
    %  Evaluation of the matrix at the Gaussian points
    A0 = A3;
    A1 = A(t+c2*h);
    A2 = A1;
    A3 = A(t+c4*h);
    
    K1 = A0 * X;
    K2 = A1 * (X + h * a21 * K1);
    K3 = A2 * (X + h * (a31 * K1 + a32 * K2));
    K4 = A3 * (X + h * (a41 * K1 + a42 * K2 + a43 * K3));
    X = X + h * (b1 * K1 + b2 * K2 + b3 * K3 + b4 * K4);
    t = t + h;
end
result = X;
end