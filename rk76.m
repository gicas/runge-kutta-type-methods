function [result, cost, name] = rk76(f, t_span, init_val, n_steps)
%RK76 solves a DE using Runge Kutta 6th-order 7-stage method
%   RK76 solves a differential equation of the type Y'=F(T) for TSPAN =
%   [T0; Tf] using NSTEPS with Y(T0) = INITVAL.

cost = 14;
name = '\mathrm{RK}_7^{[6]}';

t0 = t_span(1);
tf = t_span(2);
h = (tf - t0) / n_steps;

rq5 = sqrt(5);
a21 = (5 - rq5) / 10;
a31 = -rq5 / 10;
a32 = (5 + 2 * rq5) / 10;
a41 = (-15 + 7 * rq5) / 20;
a42 = (-1 + rq5) / 4;
a43 = (15 - 7 * rq5) / 10;
a51 = (5 - rq5) / 60;
a52 = 0; a53 = 1 / 6;
a54 = (15 + 7 * rq5) / 60;
a61 = (5 + rq5) / 60;
a62 = 0; a63 = (9 - 5 * rq5) / 12;
a64 = 1 / 6;
a65 = (-5 + 3 * rq5) / 10;
a71 = 1 / 6; a72 = 0;
a73 = (-55 + 25 * rq5) / 12;
a74 = (-25 - 7 * rq5) / 12;
a75 = 5 - 2 * rq5;
a76 = (5 + rq5) / 2;
c2 = (5 - rq5) / 10;
c3 = (5 + rq5) / 10;
c4 = c2; c5 = c3; c6 = c2;
c7 = 1;
b1 = 1 / 12;
b2 = 0;
b3 = 0;
b4 = 0;
b5 = 5 / 12;
b6 = 5 / 12;
b7 = 1 / 12;

I = eye(size(f(0)));
Z = zeros(size(f(0)));
A = @(t) [[Z, I]; [f(t), Z]];

t = t0;
X = init_val;
A6 = A(t0);
for j = 1:n_steps
    %  Evaluation of the matrix at the Gaussian points
    A0 = A6;
    A1 = A(t+c2*h);
    A2 = A(t+c3*h);
    A3 = A(t+c4*h);
    A4 = A(t+c5*h);
    A5 = A(t+c6*h);
    A6 = A(t+c7*h);
    
    K1 = A0 * X;
    K2 = A1 * (X + h * a21 * K1);
    K3 = A2 * (X + h * (a31 * K1 + a32 * K2));
    K4 = A3 * (X + h * (a41 * K1 + a42 * K2 + a43 * K3));
    K5 = A4 * (X + h * (a51 * K1 + a52 * K2 + a53 * K3 + a54 * K4));
    K6 = A5 * (X + h * (a61 * K1 + a62 * K2 + a63 * K3 + a64 * K4 + a65 * K5));
    K7 = A6 * (X + h * (a71 * K1 + a72 * K2 + a73 * K3 + a74 * K4 + a75 * K5 + a76 * K6));
    X = X + h * (b1 * K1 + b2 * K2 + b3 * K3 + b4 * K4 + b5 * K5 + b6 * K6 + b7 * K7);
    t = t + h;
end
result = X;
end

