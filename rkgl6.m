function result = rkgl6(f, tSpan, initVal, nSteps)

name = 'RKGL^{[6]}';
cost = 36; %5-7 iteration 30-42 products

a11 = 5 / 36;
a12 = 2 / 9 - sqrt(15) / 15;
a13 = 5 / 36 - sqrt(15) / 30;
a21 = 5 / 36 + sqrt(15) / 24;
a22 = 2 / 9;
a23 = 5 / 36 - sqrt(15) / 24;
a31 = 5 / 36 + sqrt(15) / 30;
a32 = 2 / 9 + sqrt(15) / 15;
a33 = 5 / 36;

b1 = 5 / 18;
b2 = 4 / 9;
b3 = 5 / 18;

c1 = (5 - sqrt(15)) / 10;
c2 = 1 / 2;
c3 = (5 + sqrt(15)) / 10;

n = size(f(0));
I = eye(n);
Z = zeros(n);

t0 = tSpan(1);
tf = tSpan(2);
h = (tf - t0) / nSteps;

result = initVal;
t = t0;

for step = 1:nSteps
    I = eye(n);
    A1 = [[Z, h * I]; [h * f(t+c1*h), Z]];
    A2 = [[Z, h * I]; [h * f(t+c2*h), Z]];
    A3 = [[Z, h * I]; [h * f(t+c3*h), Z]];
    
    AA = [a11 * A1, a12 * A2, a13 * A3; ...
        a21 * A1, a22 * A2, a23 * A3; ...
        a31 * A1, a32 * A2, a33 * A3];
    bA = [b1 * A1, b2 * A2, b3 * A3];
    
    I=eye(2*n);
    temp = (I + bA * ((eye(2*3*n) - AA)\[I, I, I]'));
    result = temp * result;
    
    t = t + h;
end
end


